# Silk Color Scheme

A nice, soothing color scheme inspired by OgloTheNerd's Reacher clock.

![](https://gitlab.com/silk-color-scheme/colors/-/raw/main/showcase.png)
